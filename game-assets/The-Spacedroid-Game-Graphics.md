## **Game Graphics Details:**
These are just app preview graphics and I will deploy all graphics separately.

### **App Store Graphics:**
![app-store-graphics2](https://user-images.githubusercontent.com/3048390/34446567-1e0b2128-ed02-11e7-9a6c-2ce52f95e283.png)

### **App Icons Catalog:**
![appicon-catalog](https://user-images.githubusercontent.com/3048390/34446527-bedd3da8-ed01-11e7-92aa-88a1ed1715f9.png)

### **Game Mockup #1:**
![app-mockup1](https://user-images.githubusercontent.com/3048390/34446472-46e1562c-ed01-11e7-879c-db9992b19c12.png)

### **Game Mockup #2:**
![app-mockup2](https://user-images.githubusercontent.com/3048390/34446492-724ccdbe-ed01-11e7-9dcb-61e523a9582a.png)

### **Falling Objects:**
![objects-preview](https://user-images.githubusercontent.com/3048390/34446722-73863628-ed03-11e7-9a16-b7e767259383.png)

### **Spaceships:**
![spaceships-preview](https://user-images.githubusercontent.com/3048390/34446731-884bc294-ed03-11e7-89b5-05e0faa57778.png)

### **App BG #1:**
![app-bg1-preview](https://user-images.githubusercontent.com/3048390/34446753-ca956790-ed03-11e7-917e-8baed34fb751.png)

### **App BG #2:**
![app-bg2-preview](https://user-images.githubusercontent.com/3048390/34446758-d327cc40-ed03-11e7-8290-b29028d7069f.png)












